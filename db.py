import dbModele as mod
import sqlite3 as sq
import unittest
import os

class Db:
    def __init__(self,aDbName):
        self.dbName = aDbName
        self.connexion = sq.connect(aDbName)
        self.createTablePersonne()

    def __del__(self):
        self.connexion.close()
       
    def createTablePersonne(self):
        cursor = self.connexion.cursor()
        cursor.execute("""CREATE TABLE IF NOT EXISTS personne
(nom text, prenom text, mail text, tel text)""")
        self.connexion.commit()

    def insertPersonne(self,unePersonne):
        if not isinstance(unePersonne, mod.Personne):
            raise TypeError("unePersonne doit être du type Personne!")
        else:
            cursor = self.connexion.cursor()
            valeurs = (unePersonne.nom, unePersonne.prenom, unePersonne.mail, unePersonne.tel)
            cursor.execute("""INSERT INTO personne(nom,prenom,mail,tel)
VALUES( ?, ?, ?, ? )""", valeurs)
            self.connexion.commit()

    def getAllPersonne(self):
        retour = []
        cursor = self.connexion.cursor()
        cursor.execute("SELECT * FROM personne")
        lesPersonnes = cursor.fetchall()
        for unePers in lesPersonnes:
            retour.append(mod.Personne(unePers[0],unePers[1],unePers[2],unePers[3]))
        return retour

    def getPersonneByName(self, aPersName):
        '''Recherche dans les nom et les prénom pour des chaînes similaires'''
        retour = []
        if (not isinstance(aPersName, str)):
            raise TypeError("Nom de la personne n'est pas une chaine")
        else:
            cursor = self.connexion.cursor()
            cursor.execute("SELECT * FROM personne where nom like ? OR prenom like ?", ('%'+aPersName+'%','%'+aPersName+'%'))
            lesPersonnes = cursor.fetchall()
            for unePers in lesPersonnes:
                retour.append(mod.Personne(unePers[0],unePers[1],unePers[2],unePers[3]))
        return retour
    
    def getPersonneByAttr(self, aPerAtt):
        '''Recherche dans les nom et les prénom pour des chaînes similaires'''
        retour = []
        if (not isinstance(aPerAtt, str)):
            raise TypeError("Nom de la personne n'est pas une chaine")
        else:
            cursor = self.connexion.cursor()
            cursor.execute('''SELECT *
                            FROM personne
                            where nom like ?
                            OR prenom like ?
                            OR mail like ?
                            OR tel like ?''',
                           ('%'+aPerAtt+'%','%'+aPerAtt+'%','%'+aPerAtt+'%','%'+aPerAtt+'%'))
            lesPersonnes = cursor.fetchall()
            for unePers in lesPersonnes:
                retour.append(mod.Personne(unePers[0],unePers[1],unePers[2],unePers[3]))
        return retour
    

class TestDb(unittest.TestCase):
    def setUp(self):
        self.rando = mod.Personne('a','b','c@c.c','0591653235')
        self.jeanMi = mod.Personne('Mi','Jean','jm@c.c','0791653235')
        self.mydb = Db(":memory:")
    def tearDown(self):
        del self.mydb
        
    def test_Init(self):
        self.assertEqual(self.mydb.dbName,":memory:")
        with self.assertRaises(TypeError):
            stuff = Db(56456)

    def test_TableCreation(self):
        cur = self.mydb.connexion.cursor()
        cur.execute("SELECT * FROM personne")
        self.assertTrue( isinstance(cur.fetchall(),list) )

    def test_Insert(self):
        self.assertRaises(TypeError ,self.mydb.insertPersonne,0)
        self.mydb.insertPersonne(self.jeanMi)
        cur = self.mydb.connexion.cursor()
        cur.execute("SELECT * FROM personne")
        unePers = cur.fetchone()
        self.assertEqual(unePers[0],self.jeanMi.nom)
        self.assertEqual(unePers[1],self.jeanMi.prenom)
        self.assertEqual(unePers[2],self.jeanMi.mail)
        self.assertEqual(unePers[3],self.jeanMi.tel)

    def test_getAll(self):
        self.mydb.insertPersonne(self.jeanMi)
        self.mydb.insertPersonne(self.rando)
        lesPers = self.mydb.getAllPersonne()
        self.assertEqual(lesPers[0],self.jeanMi)
        self.assertEqual(lesPers[1],self.rando)

    def test_getByName(self):
        self.mydb.insertPersonne(self.jeanMi)
        self.mydb.insertPersonne(self.rando)
        self.assertRaises(TypeError,self.mydb.getPersonneByName,0)

        #Doit retourner les deux lignes
        lesPers = self.mydb.getPersonneByName("a")
        self.assertEqual(len(lesPers),2)
        self.assertEqual(lesPers[0],self.jeanMi)
        self.assertEqual(lesPers[1],self.rando)
        
        #Doit retourner jeanMi
        lesPers = self.mydb.getPersonneByName("Mi")
        self.assertEqual(len(lesPers),1)
        self.assertEqual(lesPers[0],self.jeanMi)
        
        #Doit retourner rando
        lesPers = self.mydb.getPersonneByName("b")
        self.assertEqual(len(lesPers),1)
        self.assertEqual(lesPers[0],self.rando)

    def test_getByAttr(self):
        self.mydb.insertPersonne(self.jeanMi)
        self.mydb.insertPersonne(self.rando)
        self.assertRaises(TypeError,self.mydb.getPersonneByAttr,0)

        #Doit retourner les deux lignes
        lesPers = self.mydb.getPersonneByAttr("c.c")
        self.assertEqual(len(lesPers),2)
        self.assertEqual(lesPers[0],self.jeanMi)
        self.assertEqual(lesPers[1],self.rando)

        #Doit retourner rando
        lesPers = self.mydb.getPersonneByAttr("05916")
        self.assertEqual(len(lesPers),1)
        self.assertEqual(lesPers[0],self.rando)
            
        
if __name__ == '__main__':
    unittest.main()
