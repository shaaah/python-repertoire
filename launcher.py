import os
import ui
import db
import unittest

class LauncherRepertoire:
    def __init__(self,nomBase):
        if (not isinstance(nomBase, str)):
            raise Exception("Le nom de la base n'est pas une chaine de charactères")
        self.monUI = ui.RepertoireUI()
        self.myDb = db.Db(nomBase)
    def bouclePrincipale(self):
        while 1:
            self.monUI.menuPrincipal()
            iIn = self.monUI.getChoixInput()
            if self.gerePrincipalInput(iIn) == 0:
                break
    def gerePrincipalInput(self,unEntier):
        if not isinstance(unEntier, int):
            raise Exception("gerePrincipalInput prend un entier en paramètre!")
        else:
            if unEntier == 1:
                self.affichePers()
            elif unEntier == 2:
                self.ajoutPers()
            elif unEntier == 3:
                aTrouver = self.monUI.getChoixRecherche()
                listTrouve = self.myDb.getPersonneByName(aTrouver)
                self.monUI.afficheListPers(listTrouve)
            elif unEntier == 4:
                aTrouver = self.monUI.getChoixRechercheAttr()
                listTrouve = self.myDb.getPersonneByAttr(aTrouver)
                self.monUI.afficheListPers(listTrouve)
            elif unEntier == 5:
                return 0
            return 1
    def affichePers(self):
        listPers = self.myDb.getAllPersonne()
        self.monUI.afficheListPers(listPers)
    def ajoutPers(self):
        maPers = self.monUI.ajoutPers()
        if maPers:
            self.myDb.insertPersonne(maPers)

if __name__ == '__main__':
    monLauncher = LauncherRepertoire('repertoire2')
    monLauncher.bouclePrincipale()
