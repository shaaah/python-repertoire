import dbModele as mod
import unittest
import test.support as ts

class RepertoireUI:
    def __init__(self):
        print('\nRépertoire\n')
    def menuPrincipal(self):
        print('Choisisez parmi les options suivantes')
        print('1 : Consulter le répertoire')
        print('2 : Ajouter une personne au répertoire')
        print('3 : Rechercher une personne')
        print('4 : Rechercher une personne (avancé)')
        print('5 : Quitter')
    def getChoixInput(self):
        try :
            myNum = int(input('>'))
            return myNum
        except ValueError:
            print('\n','''/!\ La valeur à entrer doit être un nombre entier!''','\n')
    def getChoixRecherche(self):
        return input("Entrez (en partie ou complet) un nom ou prénom\n>")
    def getChoixRechercheAttr(self):
        return input("Entrez (en partie ou complet) un nom, prénom, numéro de téléphone ou une adresse mail\n>")
    def afficheListPers(self,lesPers):
        if not isinstance(lesPers, list):
            print('\n','''/!\ afficheListPers doit recevoir une liste de personne!''')
        elif not (len(lesPers)>0):
            print('\n','Rien à afficher','\n')
            return 0
        elif not isinstance(lesPers[0], mod.Personne):
            print('\n','''/!\ afficheListPers doit recevoir une liste de personne!''')
        else:
            print('\n','\n'.join(str(aPers) for aPers in lesPers),'\n',sep='')            
    def ajoutPers(self):
        print('''Ajout d'une personne : ''')
        print('''\tEntrez un nom''')
        unNom = input('>')
        print('''\tEntrez un prenom''')
        unPrenom = input('>')
        print('''\tEntrez une adresse e-mail''')
        unMail = input('>')
        print('''\tEntrez un numéro de téléphone''')
        unTel = input('>')
        try:
            return mod.Personne(unNom, unPrenom, unMail, unTel)
        except Exception as m:
            print(m)
            return 0
        
class TestUi(unittest.TestCase):
    def setUp(self):
        self.rep = RepertoireUI()
        self.testPers = mod.Personne('na', 'ne', 'nane@o.o', '3245980616')

    def test_ChoixInput(self):
        with ts.captured_stdin() as stdin, ts.captured_stdout() as stdout:
            stdin.write('956\n')
            stdin.write('a\n')
            stdin.write('3.56\n')
            stdin.seek(0)
            a = self.rep.getChoixInput()
            self.assertEqual(a,956)
            self.rep.getChoixInput()
            self.assertIn('/!\\',stdout.getvalue())
            self.rep.getChoixInput()
            self.assertIn('/!\\',stdout.getvalue())

    def test_Ajout(self):
        with ts.captured_stdin() as stdin, ts.captured_stdout() as stdout:
            stdin.write(self.testPers.nom+'\n')
            stdin.write(self.testPers.prenom+'\n')
            stdin.write(self.testPers.mail+'\n')
            stdin.write(self.testPers.tel+'\n')
            stdin.seek(0)
            retourPers = self.rep.ajoutPers()
        self.assertEqual(retourPers,self.testPers)

    def test_AfficheListe(self):
        with ts.captured_stdout() as stdout:
            self.rep.afficheListPers([0,5,6])
            self.assertIn('/!\\',stdout.getvalue())
            self.rep.afficheListPers(0)
            self.assertIn('/!\\',stdout.getvalue())
            self.rep.afficheListPers([])
            self.assertIn('Rien à afficher',stdout.getvalue())
            self.rep.afficheListPers([self.testPers])
            self.assertIn('na ne nane@o.o 3245980616',stdout.getvalue())
            

if __name__ == '__main__':
    unittest.main()


    
