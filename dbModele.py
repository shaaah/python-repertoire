import unittest
import re

class Personne:
    def __init__(self,anom,aprenom,amail,atel):
        self.nom = anom
        self.prenom = aprenom
        self.mail = amail
        self.tel = atel
            
    def __str__(self):
        return self.nom+ " " +self.prenom+ " " +self.mail+ " " +self.tel

    def __eq__(self,other):
        return self.nom == other.nom and self.prenom == other.prenom and self.mail == other.mail and self.tel == other.tel

    @property
    def tel(self):
        return self._tel

    @tel.setter
    def tel(self,value):
        if not isinstance(value, str):
            raise TypeError("La variable tel doit être une chaîne de caractères")
        elif not re.match('''^[\d\+ ]{10,12}$''',value):
            raise ValueError("Le numéro de téléphone donné n'est pas correct")
        else:
            self._tel = value
            
    @property
    def mail(self):
        return self._mail

    @mail.setter
    def mail(self,value):
        if not isinstance(value, str):
            raise TypeError("La variable mail doit être une chaîne de caractères")
        elif not re.match('''^.+@.+\..+$''',value):
            raise ValueError("Le mail donné n'est pas correct")
        else:
            self._mail = value
            
    @property
    def nom(self):
        return self._nom

    @nom.setter
    def nom(self,value):
        if not isinstance(value, str):
            raise TypeError("La variable nom doit être une chaîne de caractères")
        elif not re.match('''^[a-zA-Zéèçàâêûîôäëÿüïö'\- ]+$''',value):
            raise ValueError("Le nom donné contient des caractères non permis ou n'est pas assez long")
        else:
            self._nom = value
    
    @property
    def prenom(self):
        return self._prenom
    
    @prenom.setter
    def prenom(self,value):
        if not isinstance(value, str):
            raise TypeError("La variable prenom doit être une chaîne de caractères")
        elif not re.match('''^[a-zA-Zéèçàâêûîôäëÿüïö'\- ]+$''',value):
            raise ValueError("Le prenom donné contient des caractères non permis ou n'est pas assez long")
        else:
            self._prenom = value


class TestPersonne(unittest.TestCase):
    def setUp(self):
        self.pers = Personne("a", "b", "c@c.c", "0591653235")
    def tearDown(self):
        del self.pers

    def testNom(self):
        self.assertEqual(self.pers.nom, "a")
        with self.assertRaises(TypeError):
            self.pers.nom = 0
        with self.assertRaises(ValueError):
            self.pers.nom = "."

    def testPrenom(self):
        self.assertEqual(self.pers.prenom, "b")
        with self.assertRaises(TypeError):
            self.pers.prenom = 0
        with self.assertRaises(ValueError):
            self.pers.prenom = "."
            
    def testMail(self):
        self.assertEqual(self.pers.mail, "c@c.c")
        with self.assertRaises(TypeError):
            self.pers.mail = 0
        with self.assertRaises(ValueError):
            self.pers.mail = "c@c"
        with self.assertRaises(ValueError):
            self.pers.mail = "c.c"
        with self.assertRaises(ValueError):
            self.pers.mail = "c"
            
    def testTel(self):
        self.assertEqual(self.pers.tel, "0591653235")
        with self.assertRaises(TypeError):
            self.pers.tel = 0
        with self.assertRaises(ValueError):
            self.pers.tel = "591653235"
        with self.assertRaises(ValueError):
            self.pers.tel = "05 91 65 32 35"
        with self.assertRaises(ValueError):
            self.pers.tel = "+33 591653235"

    def testEq(self):
        autre = Personne("a", "b", "c@c.c", "0591653235")
        self.assertTrue(self.pers == autre)

if __name__ == '__main__':
    unittest.main()
